<!DOCTYPE html>
<html>

<head lang="es">
	<meta charset="utf-8">
	<title>Trabajo en grupo TG3</title>
	<link rel="stylesheet" href="style.css">
	<style>
		table,
		th,
		td {
			border: 1px solid black;
		}
	</style>
</head>

<body>
	<header>
		<h1>Desarrollo con Tecnologias Emergentes. Curso 2023-24<br>
			Trabajo TG3 del grupo ??<br>
			Titulo: TG3. Desarrollo de prototipos y evaluación con tecnologías emergentes</h1>
	</header>
	<nav>
		<ul>
			<li><a href="#1">1. Informacion general</a></li>
			<li><a href="#2">2. Requisitos de los prototipos a implementar</a></li>
			<li><a href="#3">3. Criterios de comparacion de los prototipos</a></li>
			<li><a href="#4">4. Implementación del prototipo con tecnología A</a></li>
			<li><a href="#5">5. Implementación del prototipo con tecnología B</a></li>
			<li><a href="#6">6. Comparacion de los prototipos</a></li>
			<li><a href="#7">7. Conclusiones</a></li>
			<li><a href="#8">8. Anexos</a></li>
		</ul>
	</nav>
		<section id=1">
			<h2>1. Informacion general</h2>
			<h3>1.1 Autores</h3>
			<p>En este apartado se debe indicar el nombre del grupo y los nombres de los autores poniendo en primer
				lugar al coordinador (Product Owner) del grupo.</p>
			<h3>1.2 Proyecto Scrum y repositorio en GitLab</h3>
			<h3>1.3 Burndown chart</h3>
			<img src="burndownTG3.jpg">
			<h3>1.4 Presentacion del trabajo</h3>
			<a href="https://url">Presentacion con diapositivas del trabajo TG3</a></p>
			<h2>2. Requisitos de los prototipos a implementar</h2>
			<h3>2.1 Catálogo de requisitos</h3>
			<table>
				<tr>
					<th>Req.</th>
					<th>Descripcion</th>
				</tr>
				<tr>
					<td>01</td>
					<td>....</td>
				</tr>
				<tr>
					<td>02</td>
					<td>....</td>
				</tr>
				<tr>
					<td>...</td>
					<td>....</td>
				</tr>
			</table>
			<h2>3. Criterios de comparacion de los prototipos</h2>
			<h3>3.1 Criterio 1: Nombre del criterio</h3>
			<ul>
				<li>Nombre del criterio: Tiempo de creación de un diagrama de clases.</li>
				<li>Descripcion: Horas invertidas en la creación del diagrama de clases utilizando el editor de la
					herramienta.</li>
				<li>Tipo de valor: Numérico (horas).</li>
			</ul>
			<h3>3.2 Criterio 2: ...</h4>
				<ul>
					<li>Nombre del criterio: ...</li>
					<li>Descripcion: ...</li>
					<li>Tipo de valor: ...</li>
				</ul>
			<h2>4. Implementación del prototipo con tecnología A</h2>
			<h2>5. Implementación del prototipo con tecnología B</h2>
			<h2>6. Comparacion de los prototipos</h2>
			<table>
				<tr>
					<th>Criterio</th>
					<th>Prototipo A</th>
					<th>Prototipo B</th>
					<th>Comentarios</th>
				</tr>
				<tr>
					<td>Nombre del criterio 1</td>
					<td>valor</td>
					<td>valor</td>
					<td></td>
				</tr>
				<tr>
					<td>Nombre del criterio 2</td>
					<td>valor</td>
					<td>valor</td>
					<td></td>
				</tr>
				<tr>
					<td>...</td>
					<td>...</td>
					<td>...</td>
					<td></td>
				</tr>
			</table>
			<h2>7. Conclusiones</h2>
			<p>A partir de la información incluida en el apartado 7 y de la experiencia al realizar el trabajo, el grupo
				debe estar en condiciones de manifestar su opinión sobre la implementación de dos prototipos similares
				pero usando dos tecnologías diferentes.
				Hay que indicar las ventajas e inconvenientes más relevantes
				de utilizar una u otra tecnología para implementar el prototipo.</p>
			<h2>8. Anexos</h2>
			<p>Hay que cumplir la estructura basica indicada de secciones anterior. En este punto si desea se pueden añadir otras
			secciones como anexos. Por ejemplo, bibliografía, alguna encuesta de opinion realizada sobre las tecnologías, 
				explicaciones sobre la utilización de recursos relevantes, etc.</p>
			<h3>Anexo 1: título</h3>
			<h3>Anexo 2: título</h3>
</body>
</html>